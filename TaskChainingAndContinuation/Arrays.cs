﻿namespace TaskChainingAndContinuation
{
    public class Arrays
    {
        public int[] CreateArray()
        {
            int[] array = new int[10];
            Random rnd = new Random();

            return array.Select(x => rnd.Next(-1000, 1000)).ToArray();
        }

        public int[] MultiplyArray(int[] array, int randomNumber)
        {
            ValidateInputArray(array);
            return array.Select(x => x * randomNumber).ToArray();
        }

        public int[] SortArray(int[] array)
        {
            return array.OrderBy(x => x).ToArray();
        }

        public double AverageArray(int[] array)
        {
            return array.Average();
        }

        private static void ValidateInputArray(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException("Array can't be empty");
            }

            foreach (int item in array)
            {
                if (item < -1000 || item > 1000)
                {
                    throw new ArgumentException($"Number have to be in range <-1000,1000>");
                }
            }
        }

    }
}