using TaskChainingAndContinuation;

namespace TaskChaining.Tests
{
    public class Tests
    {
        [Test]
        public void CreatingArrayTest()
        {
            Arrays arr = new Arrays();
            var array = arr.CreateArray();
            Assert.That(array.Length, Is.EqualTo(10));
        }

        [TestCase(new[] { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }, 2, ExpectedResult = new[] { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 })]
        [TestCase(new[] { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 }, 2, ExpectedResult = new[] { 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 })]
        public int[] MultipyArrayTest(int[] array, int randomNumber)
        {
            Arrays arr = new Arrays();
            return arr.MultiplyArray(array, randomNumber);
        }

        [TestCase(new[] { 2, 1, 4, 3, 9, 8, 7, 5, 6, 0 }, ExpectedResult = new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 })]
        [TestCase(new[] { -4, 2, 1, -3, 0, -2, -1, -5, 3, 4 }, ExpectedResult = new[] { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 })]
        public int[] SortArrayTest(int[] array)
        {
            Arrays arr = new Arrays();
            return arr.SortArray(array);
        }

        [TestCase(new[] { 2, 1, 4, 3, 9, 8, 7, 5, 6, 0 }, ExpectedResult = 4.5 )]
        [TestCase(new[] { -4, 2, 1, -3, 0, -2, -1, -5, 3, 4 }, ExpectedResult = -0.5 )]
        public double AverageArrayTest(int[] array)
        {
            Arrays arr = new Arrays();
            return arr.AverageArray(array);
        }
    }
}