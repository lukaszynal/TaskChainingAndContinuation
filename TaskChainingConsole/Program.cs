﻿using TaskChainingAndContinuation;

namespace TaskChainingConsole
{
    public static class Program
    {
        static void Main()
        {
            MainTask();
        }

        public static void MainTask()
        {
            Arrays arr = new Arrays();
            Random rnd = new Random();

            Task<int[]> taskCreateArray = Task.Run(() =>
            {
                var result = arr.CreateArray();
                PrintingResult("Created array:", result);

                return result;
            });
            Task<int[]> taskMultiplyArray = taskCreateArray.ContinueWith(antecedent =>
            {
                var randomNumber = rnd.Next(-1000, 1000);
                var result = arr.MultiplyArray(antecedent.Result, randomNumber);
                PrintingResult("Array multiplied by", result, randomNumber);

                return result;
            });
            Task<int[]> taskSortArray = taskMultiplyArray.ContinueWith(antecedent =>
            {
                var result = arr.SortArray(antecedent.Result);
                PrintingResult("Sorted array:", result);

                return result;

            });
            Task<double> taskAverageArray = taskSortArray.ContinueWith(antecedent => arr.AverageArray(antecedent.Result));

            Console.WriteLine($"Average of array: {taskAverageArray.Result}");
        }

        private static void PrintingResult(string title, int[] array)
        {
            Console.WriteLine(title);
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine($"[{i}]: {array[i]}");
            }
        }

        private static void PrintingResult(string title, int[] array, int randomNumber)
        {
            Console.WriteLine($"{title} {randomNumber}");
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine($"[{i}]: {array[i]}");
            }
        }
    }
}